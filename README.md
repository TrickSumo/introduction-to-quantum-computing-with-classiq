# Interactive Introduction To Quantum Computing With Classiq

## QCourse551-1 - Project 26 - Group "Educators' Minds" - Mentor: Nati Erez

### Álvaro Rafael Gómez - Rishi Kumar Tiwari - Azuany Mila - Manjula Gandhi S


[Course Start](START.ipynb) &nbsp;|&nbsp;

## Bibliography

Classiq documentation - https://www.classiq.io/  -  https://docs.classiq.io/latest/  -  https://www.classiq.io/docs

Quantum Computation and Quantum Information - 10th Anniversary Edition - Michael A. Nielsen & Isaac L. Chuang

An Introduction to Quantum Computing - Phillip Kaye & Raymond Laflamme & Michele Mosca


